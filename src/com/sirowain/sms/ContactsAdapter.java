/* This file is part of Tobu.
 * 
 * Tobu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Tobu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Tobu.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.sirowain.sms;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.sirowain.sms.dm.Contact;

public class ContactsAdapter extends ArrayAdapter<Contact> {

	private LayoutInflater inflater;

	public ContactsAdapter(Context context, List<Contact> objects) {
		super(context, R.layout.contact_entry, R.id.contactEntryText, objects);
		this.inflater = LayoutInflater.from(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Contact contact = (Contact) this.getItem(position);

		// The child views in each row.
		CheckBox checkBox;
		TextView textView;

		// Create a new row view
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.contact_entry, null);

			// Find the child views.
			textView = (TextView) convertView.findViewById(R.id.contactEntryText1);
			checkBox = (CheckBox) convertView.findViewById(R.id.contactEntryText);

			convertView.setTag(new ContactViewHolder(textView, checkBox));

			checkBox.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					CheckBox cb = (CheckBox) v;
					Contact contact = (Contact) cb.getTag();
					contact.isChecked = cb.isChecked();
				}
			});
		}
		// Reuse existing row view
		else {
			ContactViewHolder viewHolder = (ContactViewHolder) convertView.getTag();
			checkBox = viewHolder.checkBox;
			textView = viewHolder.textView;
		}

		// Tag the CheckBox with the Contact it is displaying
		checkBox.setTag(contact);

		// Display Contact data
		checkBox.setChecked(contact.isChecked);
		checkBox.setText(contact.displayName);
		textView.setText(contact.phone);

		return convertView;
	}

	private static class ContactViewHolder {
		public CheckBox checkBox;
		public TextView textView;

		public ContactViewHolder(TextView textView, CheckBox checkBox) {
			this.checkBox = checkBox;
			this.textView = textView;
		}
	}
	
	public void toggleAll(boolean isChecked) {
		
	}

}
