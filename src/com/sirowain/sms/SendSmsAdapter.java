/* This file is part of Tobu.
 * 
 * Tobu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Tobu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Tobu.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.sirowain.sms;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sirowain.sms.dm.Contact;

public class SendSmsAdapter extends ArrayAdapter<Contact> {

	@Override
	public boolean isEnabled(int position) {
		return false;
	}

	private LayoutInflater inflater;

	public SendSmsAdapter(Context context, List<Contact> objects, String message) {
		super(context, R.layout.preview_entry, R.id.preview_text, objects);
		this.inflater = LayoutInflater.from(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		Contact contact = (Contact) this.getItem(position);
		TextView name;
		TextView status;
		ImageView icon;

		// Create a new row view
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.send_entry, null);
			name = (TextView) convertView.findViewById(R.id.send_text);
			status = (TextView)convertView.findViewById(R.id.send_status);
			icon = (ImageView)convertView.findViewById(R.id.send_icon);
			convertView.setTag(new ViewHolder(name, icon, status));
		} else {
			ViewHolder holder = (ViewHolder) convertView.getTag();
			name = holder.name;
			status = holder.status;
			icon = holder.icon;
		}

		// set name
		name.setText(contact.displayName);
		
		return convertView;
	}
		
	private static class ViewHolder {
		public ImageView icon;
		public TextView name;
		public TextView status;

		public ViewHolder(TextView name, ImageView icon, TextView status) {
			this.name = name;
			this.status = status;
			this.icon = icon;
		}
	}
	
}
