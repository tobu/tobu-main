/* This file is part of Tobu.
 * 
 * Tobu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Tobu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Tobu.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.sirowain.sms.dm;

import java.io.Serializable;

public class Contact implements Serializable{

	private static final long serialVersionUID = 8710973362792417223L;
	
	public boolean showPreview = false;
	public boolean isChecked;
	public int replaceWith = Utils.NAME;
	
	public String phone;
	public String displayName;
	public String familyName;
	public String firstName;
	public String message;
}
