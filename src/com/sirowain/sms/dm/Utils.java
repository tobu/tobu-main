/* This file is part of Tobu.
 * 
 * Tobu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Tobu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Tobu.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.sirowain.sms.dm;

import android.content.Context;
import android.text.Html;
import android.widget.Toast;

public final class Utils {
	private static int alert_duration = Toast.LENGTH_SHORT;

	public static int NAME = 1;
	public static int SURNAME = 2;
	public static int FULLNAME = 3;
	public static String toReplace = (Html.fromHtml("&#9733;name&#9733;")).toString();
	
	public static void showAlert(Context context, int message) {
		Toast toast = Toast.makeText(context, message, alert_duration);
		toast.show();
	}
	
	public static void showAlert(Context context, String message) {
		Toast toast = Toast.makeText(context, message, alert_duration);
		toast.show();
	}
}
