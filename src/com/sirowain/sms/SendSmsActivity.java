/* This file is part of Tobu.
 * 
 * Tobu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Tobu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Tobu.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.sirowain.sms;

import java.util.ArrayList;
import java.util.Iterator;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.sirowain.sms.dm.Contact;
import com.sirowain.sms.dm.Utils;

public class SendSmsActivity extends Activity {
	Context context;
	ListView listView;
	String message;
	ToggleButton widget1;
	ToggleButton widget2;
	ToggleButton widget3;
	boolean hasFinished;
	Button btn_prev;
	Button btn_next;
	Resources res;
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	ArrayList<Contact> mArrayList;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.context = getApplicationContext();
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		res = getResources();
		
		this.setContentView(R.layout.contact_list);
		listView = (ListView)findViewById(android.R.id.list);
		
		// Custom breadcrumbs and menu
		setupBreadcrumbs();
		setupNext();
		
		// Next button on footer
		btn_next = (Button)findViewById(R.id.btn_next);
		btn_next.setEnabled(false);
		btn_next.setText("");
		btn_next.setCompoundDrawables(null,null,null,null);
		
		// Back button on footer
		btn_prev = (Button)findViewById(R.id.btn_previous);
		btn_prev.setText(R.string.cancel);
		btn_prev.setEnabled(false);
		btn_prev.setCompoundDrawablesWithIntrinsicBounds(res.getDrawable(
				R.drawable.cancel),
				null, null, null);
		btn_prev.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View arg0) {
				if (hasFinished)
				onBackPressed();
			}
		});
		
		if (getIntent().getExtras()!=null) {
			message = getIntent().getExtras().getString("com.sirowain.sms.message");
			mArrayList = (ArrayList<Contact>) (getIntent().getExtras().get("com.sirowain.sms.contacts"));
			if (mArrayList!=null) {
				SendSmsAdapter adapter = new SendSmsAdapter(this,mArrayList, message);
				listView.setAdapter(adapter);
			}
		}
		
		new SendSmsTask().execute(mArrayList);

	}
	
	public void onBack(View v) {
    	onBackPressed();
    }
    
    public void onNext(View v) {
    }

    private void setupBreadcrumbs() {
    	TextView bread1 = (TextView) findViewById(R.id.bread1);
    	bread1.setBackgroundResource(R.drawable.bread1_ok_ok);
    	bread1.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
    	
    	TextView bread2 = (TextView) findViewById(R.id.bread2);
    	bread2.setBackgroundResource(R.drawable.bread1_ok_ok);
    	bread2.setText(R.string.bread2);
    	bread2.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
    	
    	TextView bread3 = (TextView) findViewById(R.id.bread3);
    	bread3.setBackgroundResource(R.drawable.bread1_ok);
    	bread3.setText(R.string.bread3);
    	bread3.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
    	
    	TextView bread4 = (TextView) findViewById(R.id.bread4);
    	bread4.setBackgroundResource(R.drawable.bread1_on_end);
    	bread4.setText(R.string.bread4);
    }
    
    private void setupNext() {
    	Button next = (Button) findViewById(R.id.btn_next);
    	next.setText(R.string.quit);
    }
    
    private class SendSmsTask extends AsyncTask<ArrayList<Contact>, Contact, Long> {

    	@Override
    	protected Long doInBackground(ArrayList<Contact>... passed) {
    		ArrayList<Contact> contacts = passed[0];
	    	synchronized (contacts) {
	    		for (Iterator<Contact> iterator = contacts.iterator(); iterator.hasNext();) {
	    			if (!isCancelled()) {
		    			Contact contact = (Contact) iterator.next();
						try {
							contacts.wait(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						publishProgress(contact);
	    			}
				}
	    		hasFinished = true;
    		}
    		return new Long(contacts.size());
    	}
        
    	protected void onProgressUpdate(Contact... contacts) {
    		for (int i = 0; i < contacts.length; i++) {
				Contact contact = contacts[i];
				View line = listView.getChildAt(mArrayList.indexOf(contact));
				TextView status = (TextView) line.findViewById(R.id.send_status);
				status.setBackgroundResource(R.drawable.status_ok);
			}
        }

        protected void onPostExecute(Long result) {
        	
        	btn_prev.setEnabled(true);
        	btn_prev.setText(R.string.back);
        	btn_prev.setCompoundDrawablesWithIntrinsicBounds(
        			res.getDrawable(R.drawable.prev),
        			null, null, null);
        	
        	btn_next.setEnabled(false);
    		btn_next.setText(R.string.quit);
    		btn_next.setCompoundDrawablesWithIntrinsicBounds(
    				null,null,res.getDrawable(R.drawable.quit),
    				null);
    		
        	Utils.showAlert(context, "Finished.");
        }

    }
}
