/* This file is part of Tobu.
 * 
 * Tobu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Tobu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Tobu.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.sirowain.sms;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.sirowain.sms.dm.Contact;
import com.sirowain.sms.dm.Utils;

public class SmsActivity extends Activity {
	Context context;
	ArrayList<Contact> mArrayList;
	EditText message;
	ScrollView scroll;
	
	public void onNext(View v) {
		// test if compiled message box
		if (message.getText().toString().contains(Utils.toReplace)) {
			Intent intent = new Intent(context, ContactsListActivity.class);
			if (mArrayList != null) {
				intent.putExtra("com.sirowain.sms.contacts", mArrayList);
			}
			intent.putExtra("com.sirowain.sms.message", message.getText().toString());
			startActivityForResult(intent, 0);
		} else {
			Utils.showAlert(context, R.string.warning_compile);
		}
	}
	
    @SuppressWarnings("unchecked")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode==Activity.RESULT_OK) {
			if (data!=null && data.getExtras()!=null) {
				mArrayList = (ArrayList<Contact>)(data.getExtras().get("com.sirowain.sms.contacts"));
			}
		}
	}

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        setContentView(R.layout.main);
        this.context = getApplicationContext();
        
        message = (EditText) findViewById(R.id.edit_message);
        message.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				scroll.fullScroll(View.FOCUS_DOWN);
				return false;
			}
		});
        
        scroll = (ScrollView)findViewById(R.id.scroll);
        
        TextView textView = (TextView) findViewById(R.id.textHelp1);
        String source = getString(R.string.help1);
        Spanned text =  Html.fromHtml(source);
        textView.setText(text);
        
        Button btn_prev = (Button)findViewById(R.id.btn_previous);
        btn_prev.setVisibility(View.INVISIBLE);
    }
    
    public void shortcut(View btn) {
    	Editable ed = message.getEditableText();
    	String stringa = (Html.fromHtml("&#9733;name&#9733; ")).toString();
    	ed.insert(message.getSelectionStart(), stringa);
    }
}