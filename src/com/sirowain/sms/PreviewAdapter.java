/* This file is part of Tobu.
 * 
 * Tobu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Tobu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Tobu.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.sirowain.sms;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.sirowain.sms.dm.Utils;
import com.sirowain.sms.dm.Contact;

public class PreviewAdapter extends ArrayAdapter<Contact> {

	private LayoutInflater inflater;
	private String origMessage;
	LinearLayout warningHeader;
	int numWarning = 0;
	Context context;

	public PreviewAdapter(Context context, List<Contact> objects, String message) {
		super(context, R.layout.preview_entry, R.id.preview_text, objects);
		this.inflater = LayoutInflater.from(context);
		this.origMessage = message;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		Contact contact = (Contact) this.getItem(position);
		TextView name;
		TextView message;
		ToggleButton widget1;
		ToggleButton widget2;
		ToggleButton widget3;
		LinearLayout hidden;

		// Create a new row view
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.preview_entry, null);
			name = (TextView) convertView.findViewById(R.id.preview_text);
			hidden = (LinearLayout)convertView.findViewById(R.id.previewHidden);
			message = (TextView)convertView.findViewById(R.id.previewMessage);
			widget1 = (ToggleButton)convertView.findViewById(R.id.previewWidget1);
			widget1.setTag(1);
			widget2 = (ToggleButton)convertView.findViewById(R.id.previewWidget2);
			widget2.setTag(2);
			widget3 = (ToggleButton)convertView.findViewById(R.id.previewWidget3);
			widget3.setTag(3);

			convertView.setTag(new PreViewHolder(name, widget1, widget2,
					widget3, hidden, message));
		
		} else {
			PreViewHolder holder = (PreViewHolder) convertView.getTag();
			name = holder.name;
			hidden = holder.hidden;
			message = holder.message;
			widget1 = holder.widget1;
			widget2 = holder.widget2;
			widget3 = holder.widget3;
		}

		// clear warning
		clearWarning(convertView);
		// set info
		name.setText(contact.displayName);
		name.setOnClickListener(previewListener);
		// show preview
		if (contact.showPreview)
			hidden.setVisibility(View.VISIBLE);
		else
			hidden.setVisibility(View.GONE);
		// disable/enable buttons
		updateWidget(contact, message, widget1, widget2, widget3);
		if ((contact.replaceWith==Utils.SURNAME && contact.familyName == null)
				|| (contact.replaceWith==Utils.NAME && contact.firstName == null)
				|| (contact.replaceWith==Utils.FULLNAME && (contact.firstName == null
				|| contact.familyName == null))) {
			setWarningLine(convertView);
		}
		// set toggle listener
		widget1.setOnClickListener(widgetListener);
		widget2.setOnClickListener(widgetListener);
		widget3.setOnClickListener(widgetListener);
		// set position tag to widgets' parent
		convertView.setTag(R.integer.TAG_POSITION, position);
		return convertView;
	}
		
	private void setWarningLine(View line) {
		line.setBackgroundColor(Color.YELLOW);
	}
	
	private void clearWarning(View line) {
		line.setBackgroundColor(Color.TRANSPARENT);
	}
	
	public void setWarningHeader(LinearLayout warningHeader) {
		this.warningHeader = warningHeader;
	}
	
	private void updatePreview(Contact contact, TextView message) {
		String replaceWith = "???";
		if (contact.replaceWith==Utils.NAME)
			replaceWith = contact.firstName;
		else if (contact.replaceWith==Utils.SURNAME)
			replaceWith = contact.familyName;
		else if (contact.replaceWith==Utils.FULLNAME)
			replaceWith = contact.displayName;
		if (replaceWith !=null)
			contact.message = new String(origMessage.replace(
					Utils.toReplace, replaceWith));
		message.setText(contact.message);
	}
	
	private void updateWidget(Contact contact, TextView message, ToggleButton widget1,
			ToggleButton widget2, ToggleButton widget3) {
		updatePreview(contact, message);
		widget1.setEnabled(true);
		widget2.setEnabled(true);
		widget3.setEnabled(true);
		if (contact.replaceWith==Utils.NAME)
			widget1.setChecked(true);
		else 
			widget1.setChecked(false);
		if (contact.replaceWith==Utils.SURNAME)
			widget2.setChecked(true);
		else
			widget2.setChecked(false);
		if (contact.replaceWith==Utils.FULLNAME)
			widget3.setChecked(true);
		else 
			widget3.setChecked(false);
		
		if (contact.firstName == null) {
			widget1.setEnabled(false);
			widget3.setEnabled(false);
		}
		if (contact.familyName == null) {
			widget2.setEnabled(false);
			widget3.setEnabled(false);
		}
		
	}
	
	private static class PreViewHolder {
		public LinearLayout hidden;
		public TextView name;
		public TextView message;
		public ToggleButton widget1;
		public ToggleButton widget2;
		public ToggleButton widget3;

		public PreViewHolder(TextView name, ToggleButton widget1, ToggleButton widget2,
				ToggleButton widget3, LinearLayout hidden, TextView message) {
			this.name = name;
			this.widget1 = widget1;
			this.widget2 = widget2;
			this.widget3 = widget3;
			this.hidden = hidden;
			this.message = message;
		}
	}
	
	OnClickListener widgetListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			int wid_pos = (Integer) v.getTag();
			
			View widget_parent = (View) v.getParent();
			ToggleButton widget1 = (ToggleButton) widget_parent.findViewWithTag(1);
			ToggleButton widget2 = (ToggleButton) widget_parent.findViewWithTag(2);
			ToggleButton widget3 = (ToggleButton) widget_parent.findViewWithTag(3);
			
			View parent_parent = (View)widget_parent.getParent();
			View line = (View)parent_parent.getParent();
			Object line_pos = line.getTag(R.integer.TAG_POSITION);
			Contact contact = getItem((Integer) line_pos);
			
			widget1.setChecked(false);
			widget2.setChecked(false);
			widget3.setChecked(false);
			switch (wid_pos) {
			case 1:
				widget1.setChecked(true);
				contact.replaceWith = Utils.NAME;
				break;
			case 2:
				widget2.setChecked(true);
				contact.replaceWith = Utils.SURNAME;
				break;
			case 3:
				widget3.setChecked(true);
				contact.replaceWith = Utils.FULLNAME;
				break;
			default:
				break;
			}
			clearWarning(line);
			// update preview
			updatePreview(contact, (TextView) line.findViewById(R.id.previewMessage));
		}
	};

	OnClickListener previewListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			View parent = (View) v.getParent();
			View parent_parent = (View)parent.getParent();
			LinearLayout hidden = (LinearLayout) parent_parent.findViewById(R.id.previewHidden);
			Contact contact = getItem((Integer) parent_parent.getTag(R.integer.TAG_POSITION));

			if (hidden.getVisibility()==View.VISIBLE) {
				hidden.setVisibility(View.GONE);
				contact.showPreview = false;
			} else {
				hidden.setVisibility(View.VISIBLE);
				contact.showPreview = true;
			}
		}
	};
}
