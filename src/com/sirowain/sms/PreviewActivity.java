/* This file is part of Tobu.
 * 
 * Tobu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Tobu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Tobu.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.sirowain.sms;

import java.util.ArrayList;
import java.util.Iterator;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.WrapperListAdapter;

import com.sirowain.sms.dm.Contact;
import com.sirowain.sms.dm.Utils;

public class PreviewActivity extends ListActivity {
	Context context;
	ListView listView;
	String message;
	ToggleButton widget1;
	ToggleButton widget2;
	ToggleButton widget3;
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	ArrayList<Contact> mArrayList;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.context = getApplicationContext();
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		LayoutInflater inflater = LayoutInflater.from(context);
		
		this.setContentView(R.layout.contact_list);
		listView = (ListView)findViewById(android.R.id.list);
		LinearLayout header = (LinearLayout) inflater.inflate(R.layout.preview_header, null);
		listView.addHeaderView(header);
		
		// header widget
		widget1 = (ToggleButton)findViewById(R.id.header_button1);
		widget1.setTag(1);
		widget1.setOnClickListener(headerListener);
		widget2 = (ToggleButton)findViewById(R.id.header_button2);
		widget2.setTag(2);
		widget2.setOnClickListener(headerListener);
		widget3 = (ToggleButton)findViewById(R.id.header_button3);
		widget3.setTag(3);
		widget3.setOnClickListener(headerListener);
		
		// Custom breadcrumbs and menu
		setupBreadcrumbs();
		setupNext();
		
		// Back button on footer
		Button btn_prev = (Button)findViewById(R.id.btn_previous);
		btn_prev.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View arg0) {
				onBackPressed();
			}
		});
		
		if (getIntent().getExtras()!=null) {
			message = getIntent().getExtras().getString("com.sirowain.sms.message");
			mArrayList = (ArrayList<Contact>) (getIntent().getExtras().get("com.sirowain.sms.contacts"));
			if (mArrayList!=null) {
				PreviewAdapter adapter = new PreviewAdapter(this,mArrayList, message);
				adapter.setWarningHeader((LinearLayout) header.getChildAt(0));
				listView.setAdapter(adapter);
			}
		}
	}
	
	public void onBack(View v) {
    	onBackPressed();
    }
    
    public void onNext(View v) {
    	Resources res = getResources();
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setMessage(res.getQuantityString(R.plurals.warning_send, 
    			mArrayList.size(), mArrayList.size()))
    	       .setCancelable(false)
    	       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	        	   Intent intent = new Intent(context, SendSmsActivity.class);
    	        	   intent.putExtra("com.sirowain.sms.contacts", mArrayList);
    	        	   startActivity(intent);
    	           }
    	       })
    	       .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	        	   dialog.cancel();
    	           }
    	       });
    	AlertDialog dialog = builder.create();
    	dialog.show();
    }

    private void setupBreadcrumbs() {
    	TextView bread1 = (TextView) findViewById(R.id.bread1);
    	bread1.setBackgroundResource(R.drawable.bread1_ok_ok);
    	bread1.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
    	
    	TextView bread2 = (TextView) findViewById(R.id.bread2);
    	bread2.setBackgroundResource(R.drawable.bread1_ok);
    	bread2.setText(R.string.bread2);
    	bread2.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
    	
    	TextView bread3 = (TextView) findViewById(R.id.bread3);
    	bread3.setBackgroundResource(R.drawable.bread1_on);
    	bread3.setText(R.string.bread3);
    }
    
    private void setupNext() {
    	Button next = (Button) findViewById(R.id.btn_next);
    	next.setText(R.string.bread4);
    }

   OnClickListener headerListener = new OnClickListener() {
	
	@Override
	public void onClick(View v) {
		int pos = (Integer) v.getTag();
		for (Iterator<Contact> iterator = mArrayList.iterator(); iterator
				.hasNext();) {
			Contact contact = (Contact) iterator.next();
			switch (pos) {
			case 1:
				contact.replaceWith = Utils.NAME;
				break;	
			case 2:
				contact.replaceWith = Utils.SURNAME;
				break;
			case 3:
				contact.replaceWith = Utils.FULLNAME;
				break;
			}
		}
		widget1.setChecked(false);
		widget2.setChecked(false);
		widget3.setChecked(false);
		switch (pos) {
		case 1:
			widget1.setChecked(true);
			break;	
		case 2:
			widget2.setChecked(true);
			break;
		case 3:
			widget3.setChecked(true);
			break;
		}
		((PreviewAdapter)(((WrapperListAdapter)listView.getAdapter()).getWrappedAdapter())).notifyDataSetChanged();
	}
};
}
