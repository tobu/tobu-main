/* This file is part of Tobu.
 * 
 * Tobu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Tobu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Tobu.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.sirowain.sms;

import java.util.ArrayList;
import java.util.Iterator;

import android.app.Activity;
import android.app.ListActivity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Data;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.WrapperListAdapter;

import com.sirowain.sms.dm.Contact;
import com.sirowain.sms.dm.Utils;

public class ContactsListActivity extends ListActivity {
	Context context;
	ListView listView;
	String message;
	
	@Override
	public void onBackPressed() {
		onBack(null);
		super.onBackPressed();
	}

	ArrayList<Contact> mArrayList;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.context = getApplicationContext();
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		this.setContentView(R.layout.contact_list);
		listView = (ListView)findViewById(android.R.id.list);
		listView.setFastScrollEnabled(true);
		createHeader();
		
		// Custom breadcrumbs and menu
		setupBreadcrumbs();
		setupNext();
		
		// Back button on footer
		Button btn_prev = (Button)findViewById(R.id.btn_previous);
		btn_prev.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View arg0) {
				onBackPressed();
			}
		});
		
		if (getIntent().getExtras()!=null) {
			message = getIntent().getExtras().getString("com.sirowain.sms.message");
			mArrayList = (ArrayList<Contact>) (getIntent().getExtras().get("com.sirowain.sms.contacts"));
			if (mArrayList!=null) {
				ContactsAdapter adapter = new ContactsAdapter(this,mArrayList);
				listView.setAdapter(adapter);
			} else
				populateContactList();
		}
	}
	
	private void createHeader() {
		CheckBox cb_select_all = new CheckBox(context);
		cb_select_all.setText(getString(R.string.select_all));
		cb_select_all.setTextColor(Color.GRAY);
		cb_select_all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				for (Iterator<Contact> iterator = mArrayList.iterator(); iterator
						.hasNext();) {
					Contact contact = (Contact) iterator.next();
					contact.isChecked=isChecked;
				}
				((ContactsAdapter)(((WrapperListAdapter)listView.getAdapter()).getWrappedAdapter())).notifyDataSetChanged();
			}
		});
		listView.addHeaderView(cb_select_all);
	}
	
    private void populateContactList() {
        // Build adapter with contact entries
        Cursor cursor = getContacts();
        ContentResolver cr = getContentResolver();
        Cursor pCur;
        
        mArrayList = new ArrayList<Contact>();

        while(cursor.moveToNext()) {
        	int id = cursor.getInt(0);
        	
        	pCur = cr.query(Data.CONTENT_URI,
      	          new String[] {CommonDataKinds.StructuredName.GIVEN_NAME, CommonDataKinds.StructuredName.FAMILY_NAME},
      	          Data.CONTACT_ID + "= ? AND " +
      	          Data.MIMETYPE + "='" + CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE + "'",
      	          new String[]{String.valueOf(id)}, null);

        	Contact contact = new Contact();
        	contact.phone = cursor.getString(1);
        	contact.displayName = cursor.getString(2);
        	if (pCur.moveToNext()) {
        		contact.firstName = pCur.getString(0);
        		contact.familyName = pCur.getString(1);
        	}
        	
        	mArrayList.add(contact);
 	        pCur.close();
       }

        ContactsAdapter adapter = new ContactsAdapter(this,mArrayList);
        listView.setAdapter(adapter);
    }

    private Cursor getContacts()
    {
    	return getContentResolver().query(Data.CONTENT_URI,
    	          new String[] {Data.CONTACT_ID, Phone.NUMBER, Phone.DISPLAY_NAME},
    	          Data.MIMETYPE + "='" + Phone.CONTENT_ITEM_TYPE + "'",
    	          null, Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC");
    }
    
    public void onNext(View v) {
    	Intent intent = new Intent(context, PreviewActivity.class);
    	
    	ArrayList<Contact> selected = new ArrayList<Contact>();
    	for (Iterator<Contact> iterator = mArrayList.iterator(); iterator.hasNext();) {
			Contact contact = (Contact) iterator.next();
			if (contact.isChecked)
				selected.add(contact);
		}
		if (selected.size()>0) {
			intent.putExtra("com.sirowain.sms.contacts", selected);
			intent.putExtra("com.sirowain.sms.message", message);
			startActivity(intent);
		} else {
			Utils.showAlert(context, R.string.warning_select);
		}
    }
    
    public void onBack(View v) {
    	Intent intent = new Intent();
		intent.putExtra("com.sirowain.sms.contacts", mArrayList);

	    if (getParent() == null) {
	        setResult(Activity.RESULT_OK, intent);
	    } else {
	        getParent().setResult(Activity.RESULT_OK, intent);
	    }
    }

    private void setupBreadcrumbs() {
    	TextView bread1 = (TextView) findViewById(R.id.bread1);
    	bread1.setBackgroundResource(R.drawable.bread1_ok);
    	bread1.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
    	
    	TextView bread2 = (TextView) findViewById(R.id.bread2);
    	bread2.setBackgroundResource(R.drawable.bread1_on);
    	bread2.setText(R.string.bread2);
    }
    
    private void setupNext() {
    	Button next = (Button) findViewById(R.id.btn_next);
    	next.setText(R.string.bread3);
    }
}
